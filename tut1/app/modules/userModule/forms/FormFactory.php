<?php
namespace App\UserModule\Model;

use \Nette\Application\UI\Form;

class FormFactory
{
  public function createAddForm(): Form
  {
    $f = new Form();

    $f->addText('username', 'Username:')
      ->setRequired(true);

    $f->addText('email', 'Email:')
      ->setRequired(true);

    $f->addSubmit('submit', 'OK');
    return $f;
  }
}
