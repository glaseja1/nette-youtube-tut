<?php
namespace Test;

use Tester;
use Nette;
use Tester\Assert;

$container = require __DIR__ . '/bootstrap.php';
/**
 * Class SomeTest
 * @testCase
 */
class SomeTest extends Tester\TestCase
{
  public function __construct()
  {}
  public function setUp()
  { }
  public function testSomething()
  {
    Assert::same(true, true);
  }
}

$t = new SomeTest();
$t->runTest('testSomething');
