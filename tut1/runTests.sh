#!/bin/bash

function clearCache()
{
  if [ ! -d ./temp/cache ]; then
    echo "[!] Note: directory ./temp/cache not found. Will not delete cache....";
  else
    rm -r ./temp/cache/*
  fi
}

##########################################

clearCache

echo '[!] note: in case fo trouble, make sure you delete the cache manually!'
./vendor/bin/tester -C tests

clearCache

exit 0
