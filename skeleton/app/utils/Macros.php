<?php

namespace App\Utils;

use Latte;
use Latte\Macros\MacroSet;
use Latte\Compiler;
use Latte\PhpWriter;
use Latte\MacroNode;
use Latte\CompileException;

class Macros extends MacroSet
{
   public static function install(Compiler $compiler)
   {
      $set = new static($compiler);
      
      $set->addMacro('label', [$set, 'macroLabel'], [$set, 'macroLabelEnd'], NULL, self::AUTO_EMPTY);
      /*  $set->addMacro('printIfExistsInArray', array($set, 'macroprintIfExistsInArray'));*/
      return $set;
   }
   
   /**
    * Adds a class 'required' a label, which is related to a required form input
    *
    * Must be public
    * @param MacroNode $node
    * @param PhpWriter $writer
    * @return [type]
    * @throws CompileException
    */
   public function macroLabel(MacroNode $node, PhpWriter $writer)
   {
      if ($node->modifiers)
         throw new CompileException('Modifiers are not allowed in ' . $node->getNotation());
      
      $words = $node->tokenizer->fetchWords();
      if (!$words)
         throw new CompileException('Missing name in ' . $node->getNotation());
      
      $node->replaced = true;
      $name = array_shift($words);
      return $writer->write(
          ($name[0] === '$'
              ? '$_input = is_object(%0.word) ? %0.word : end($this->global->formsStack)[%0.word];'
              : '$_input = $_form[%0.word];') . '
        $attributes = %node.array;

        if (!isset($_input->required))
          return;

        if ($_input->required) {
          if(array_key_exists("class", $attributes)) {
            $attributes["class"] .= " required";
          }
          else {
            $attributes += array("class" => "required");
          }
        }
        if ($_label = $_input->%1.raw)
          echo $_label->addAttributes($attributes)', $name,
          $words ? ('getLabelPart(' . implode(', ', array_map([$writer, 'formatWord'], $words)) . ')') : 'getLabel()');
   }
   
   /**
    * Must be public
    * @param MacroNode $node
    * @param PhpWriter $writer
    * @return string [type]
    */
   public function macroLabelEnd(MacroNode $node, PhpWriter $writer)
   {
      if ($node->content != NULL) {
         $node->openingCode = rtrim($node->openingCode, '?> ') . '->startTag() ?>';
         return $writer->write('if ($_label) echo $_label->endTag()');
      }
   }
   
   /**
    * @param MacroNode $node
    * @param PhpWriter $writer
    * @return string
    */
   public function macroCurrency(MacroNode $node, PhpWriter $writer)
   {
      return $writer->write(
          'echo \App\Utils\Macros::renderMacroCurrency(%node.word)'
      );
   }
   
   /**
    * @param MacroNode $node
    * @param PhpWriter $writer
    * @return string
    */
   public function macroprintIfExistsInArray(MacroNode $node, PhpWriter $writer)
   {
      return $writer->write(
          'echo \App\Utils\Macros::renderMacroPrintIfSet(%node.word, %node.word2)'
      );
   }
   
   /**
    * @param null $value
    * @param      $key
    * @return string|null
    */
   public static function renderMacroPrintIfSet($value, $key)
   {
      if (isset($value))
         return $value;
      return '';
   }
   
   /**
    * @param null $currency
    * @return string
    */
   public static function renderMacroCurrency($currency = NULL): string
   {
      $currency = $currency ?: 0;
      
      switch ($currency) {
         case 'EUR':
            return "€";
         default:
            return 'Kč';
      }
   }
}
