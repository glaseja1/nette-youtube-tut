Nette Project Template
=================
created by Jan Glaser
=================

This is simple, working skeleton for nette web application.
It already has translator, base presenters and basic setup.

The project has also fuctioning dummy test. You can run tests by executing
`./runTests.sh`

Requirements
------------

PHP 5.6 or higher - needed for the nette core
PHP ? or higher   - needed for the PHP language: The example presenters, models, daos and classes written by me are using PHP constructs that require that version


Installation
------------

You need to make the directories `temp/` and `log/` writable
`chmod 777 ./temp`
`chmod 777 ./log`

Note: this is safe, as both temp and log can be acessed by anyone, even visitor, and it is not a security threat
