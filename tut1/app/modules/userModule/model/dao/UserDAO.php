<?php
namespace App\UserModule\Model\DAO;

class UserDAO
{
  /**
   * @var string
   */
  public $email;
  /**
  * @var int
  */
  public $id;
  /**
   * @var string
   */
  public $username;
  public function __construct()
  {}
  public static function createFromFormValues(array $values): UserDAO
  {
    $a = new UserDAO();
    $a->username = $values['username'];
    $a->email = $values['email'];
    return $a;
  }
}
