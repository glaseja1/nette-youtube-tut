<?php
namespace App\Model;

use Nette\Database\Context;

class BaseModel
{
  /**
   * @var Context
   */
  protected $database;
  public function __construct(Context $database)
  {
    $this->database = $database;
  }
}
