<?php
/*v 1.2 */
namespace App\GameManagementModule\TEMPLATE_Module\Presenters;

use App\Presenters\BasePresenter;
use Nette;
use Nette\Application\UI\Form;

use App\Components\AddViewDeleteComponent;

//presenters

//models
use App\GameManagementModule\TEMPLATE_Module\Model\TEMPLATE_Model;

//daos
use App\GameManagementModule\TEMPLATE_Module\Model\DAO\TEMPLATE_DAO;

//factories
use App\GameManagementModule\TEMPLATE_Module\Forms\TEMPLATE_FormFactory;

/**
 * Class TEMPLATE_Presenter
 * @package App\GameManagementModule\TEMPLATE_Module\Presenters
 */
class TEMPLATE_Presenter extends BasePresenter
{
   /** @var string */
   const ACL_RESOURCE = 'prayers';
   
   /** @var string */
   const NAVBAR_NAME_TOKEN = 'navBar.prayers';

   /** @var TEMPLATE_FormFactory */
   private $formFactory;

   /** @var TEMPLATE_Model */
   private $modelPrayers;

   //*****************************************************************************
   public function __construct()
   {
      parent::__construct();
   }

   public function injectDependencies(
       TEMPLATE_Model $modelPrayers,
       TEMPLATE_FormFactory $formFactory)
   {
      $this->modelPrayers = $modelPrayers;
      $this->formFactory = $formFactory;
   }

   protected function startup()
   {
      parent::startup();
      if (!$this->getUser()->isAllowed(self::ACL_RESOURCE, 'view'))
         throw new Nette\Application\ForbiddenRequestException;

      $this->template->setFile(__DIR__ . "/templates/home.latte");
   }

   //*****************************************************************************

   /**
    * @param $id
    * @throws Nette\Application\AbortException
    * @throws Nette\Application\ForbiddenRequestException
    */
   public function actionDelete($id)
   {
      if (!$this->getUser()->isAllowed(self::ACL_RESOURCE, 'delete'))
         throw new Nette\Application\ForbiddenRequestException;

      $res = $this->modelPrayers->delete($id);
      if ($res->hasErrors()) {
         foreach ($res->getErrors() as $e)
            $this->flashMessage($e, 'error');
      }
      $this->redirect($this->modelLinks->managePrayersHome->getNetteHref());
   }

   /**
    * @param $id
    * @throws Nette\Application\AbortException
    * @throws Nette\Application\ForbiddenRequestException
    */
   public function actionEdit($id)
   {
      if (!$this->user->isAllowed(self::ACL_RESOURCE, 'edit'))
         throw new Nette\Application\ForbiddenRequestException;

      $this->template->setFile(__DIR__ . "/templates/editPrayer.latte");

      $this->template->editDAO = $this->modelPrayers->find($id);
      if ($this->template->editDAO == null) {
         $this->flashMessage('Prayer with that id does not exist', 'error');
         $this->redirect($this->modelLinks->managePrayersHome->getNetteHref());
         return;
      }
   }

   /**
    * @param $id
    * @throws Nette\Application\AbortException
    * @throws Nette\Application\ForbiddenRequestException
    */
   public function actionView($id)
   {
      if (!$this->getUser()->isAllowed(self::ACL_RESOURCE, 'view'))
         throw new Nette\Application\ForbiddenRequestException;
      //get data
      $dao = $this->modelPrayers->find($id);
      if ($dao == null) {
         $this->flashMessage('Prayer with that id does not exist', 'error');
         $this->redirect($this->modelLinks->managePrayersHome->getNetteHref());
         return;
      }
      $this->template->setFile(__DIR__ . "/templates/viewEntity.latte");
      $this->currentPageTitle = $this->translate($dao->displayName);
      $this->template->viewDAO = $dao;
   }

   private function addThisPresenterToBreadcrumb()
   {
      /** @noinspection PhpUndefinedMethodInspection */
      $this['breadcrumb']->addLink($this->translate('gameManagement.text'), $this->modelLinks->managementHome->getRealHref());
      /** @noinspection PhpUndefinedMethodInspection */
      $this['breadcrumb']->addLink($this->translate(self::NAVBAR_NAME_TOKEN), $this->modelLinks->managePrayersHome->getRealHref());
   }

   public function beforeRender()
   {
      parent::beforeRender();
      $this->addThisPresenterToBreadcrumb();
   }

   /**
    * @return Form
    */
   protected function createComponentAddEditForm(): Form
   {
      if (!isset($this->template->editDAO) || $this->template->editDAO === null) {
         $form = $this->formFactory->createAddForm();
         $form->onSuccess[] = [$this, 'formSucceededAdd'];
      } else {
         $form = $this->formFactory->createEditForm($this->template->editDAO);
         $form->onSuccess[] = [$this, 'formSucceededEdit'];
      }
      return $form;
   }

   /**
    * @param Form $form
    * @param      $values
    * @throws Nette\Application\AbortException
    * @throws Nette\Application\ForbiddenRequestException
    */
   public function formSucceededAdd(Form $form, array $values)
   {
      $dao = TEMPLATE_DAO::createFromFormValues($values);

      if (!$this->getUser()->isAllowed(self::ACL_RESOURCE, 'add'))
         throw new Nette\Application\ForbiddenRequestException;

      $res = $this->modelPrayers->insert($dao);
      parent::onAddEditFormOperationDone($res, false);
      $this->redirect($this->modelLinks->managePrayersHome->getNetteHref());
   }

   /**
    * @param Form $form
    * @param      $values
    * @throws Nette\Application\AbortException
    * @throws Nette\Application\ForbiddenRequestException
    */
   public function formSucceededEdit(Form $form, array $values)
   {
      $dao = TEMPLATE_DAO::createFromFormValues($values);

      if (!$this->getUser()->isAllowed(self::ACL_RESOURCE, 'edit'))
         throw new Nette\Application\ForbiddenRequestException;

      $id = $values['editID'];
      $res = $this->modelPrayers->update($id, $dao);

      parent::onAddEditFormOperationDone($res, true);
      $this->redirect("edit", $id);
   }

   public function renderDefault()
   {
      //defaults
      $this->template->daos = $this->modelPrayers->findAll();

      //******************************************
      //create buttons
      //******************************************
      $this->template->buttonGroup = [];
      for ($i = 0; $i < count($this->template->daos); $i++) {
         $dao = $this->template->daos[$i];

         $deleteConfirmationText = "Are you sure you want to delete entity {$dao->displayName}?
          This action can not be reverted";
         $deleteLink = $this->link('delete', $dao->id);
         
         $comp = new AddViewDeleteComponent($this->translator);
         $comp->addViewButton($this->link('view', $dao->id));
         $comp->addEditButton($this->link('edit', $dao->id));
         $comp->addDeleteModalButton($deleteLink, $deleteConfirmationText, 'global-deletionModal-vf5dzg14sd');
   
         $this->template->buttonGroup[$i] = $comp;
         $this->addComponent($this->template->buttonGroup[$i], "buttonGroup{$i}");
      }
   }

   public function renderEdit()
   {
      /** @noinspection PhpUndefinedMethodInspection */
      $this['breadcrumb']->addLink($this->template->editDAO->displayName, $this->link('view', $this->template->editDAO->id));
      /** @noinspection PhpUndefinedMethodInspection */
      $this['breadcrumb']->addLink('edit', '#');
   }

   public function renderView()
   {
      /** @noinspection PhpUndefinedMethodInspection */
      $this['breadcrumb']->addLink($this->template->viewDAO->displayName, '#');
   }
}

?>
