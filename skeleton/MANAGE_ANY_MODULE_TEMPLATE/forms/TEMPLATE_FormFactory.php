<?php

namespace App\GameManagementModule\TEMPLATE_Module\Forms;

use App\Components\FormFactoryBase;
use \Nette\Application\UI\Form;
use \Kdyby\Translation\Translator;

//models
use App\GameManagementModule\TEMPLATE_Module\Model\DAO\TEMPLATE_DAO;

//form controls
use App\Components\Form\StyledCheckBox;

//factories

/**
 * Class TEMPLATE_FormFactory
 * @package App\GameManagementModule\TEMPLATE_Module\Forms
 */
class TEMPLATE_FormFactory extends FormFactoryBase
{
   const
       BASE_TRANS_TOKEN = "gameManagement.addMonster.form.";
   
   /**
    * TEMPLATE_FormFactory constructor.
    * @param Translator $translator
    */
   public function __construct(Translator $translator)
   {
      parent::__construct($translator);
   }
   
   /**
    * @return Form
    */
   public function createAddForm(): Form
   {
      $form = parent::createStyledForm();
      
      $textTranslated = ucfirst($this->translate(self::BASE_TRANS_TOKEN . 'displayNameToken'));
      $form->addText('displayName', "{$textTranslated}:")
          ->setRequired('required')
          ->setValue('game.prayer.?');
      
      $textTranslated = ucfirst($this->translate('game.level'));
      $form->addInteger('levelReq', "{$textTranslated}:")
          ->setRequired('required')
          ->setDefaultValue(0)
          ->addRule($form::RANGE,
              $this->getErrorMessageNumberInRange(), [1, 1000]);
      
      $form->addInteger('pointConsumMs', "Point consumption delay (ms):")
          ->setRequired('required')
          ->setDefaultValue(1000)
          ->addRule($form::RANGE,
              $this->getErrorMessageNumberInRange(), [1, 1000 * 60]);
      
      $textTranslated = $this->translate(self::BASE_TRANS_TOKEN . 'examineText');
      $form->addTextArea('description', "{$textTranslated}:")
          ->setRequired('required')
          ->setValue('game.prayer.?.desc');
      
      $textTranslated = ucfirst($this->translate('gameManagement.abilities.codeTag'));
      $form->addText('inCodeTAG', $textTranslated . ':');
      
      $textTranslated = $this->translate(self::BASE_TRANS_TOKEN . 'resourceDir');
      $form->addText('resourceDir', "{$textTranslated}:")
          ->setRequired('required');
      
      $form['overhead'] = new StyledCheckBox($this->translate('gameManagement.prayers.overhead'));
      
      $form->addSubmit('submit', $this->translate('general.formAdd'));
      return $form;
   }
   
   /**
    * @param TEMPLATE_DAO $dao
    * @return Form
    */
   public function createEditForm(TEMPLATE_DAO $dao): Form
   {
      $form = $this->createAddForm();
      
      $form->setValues([
          'displayName' => $dao->displayName,
          'resourceDir' => $dao->resourceDir,
          'levelReq' => $dao->levelReq,
          'overhead' => $dao->overhead,
          'inCodeTAG' => $dao->codeClass,
          'pointConsumMs' => $dao->pointConsumptionMs
      ]);
      $form->addHidden('editID', $dao->id);
      parent::setSubmitButtonCaptionUpdate($form);
      return $form;
   }
}
