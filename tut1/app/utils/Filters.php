<?php

class Filters
{
   public static function common($filter, $value)
   {
      if (method_exists(__CLASS__, $filter)) {
         $args = func_get_args();
         array_shift($args);
         return call_user_func_array(array(__CLASS__, $filter), $args);
      }
   }

   /**
    * Given miliseconds, returns string describing the time as hurs, minutes, seconds, miliseconds
    * @param  $input
    * @return
    */
   public static function msToReadable($input): string
   {
      if ($input == 0)
         return "0s";

      $workVal = $input;
      $h = floor($workVal / (3600 * 1000));
      $workVal -= $h * 3600 * 1000;
      $m = floor($workVal / (60 * 1000));
      $workVal -= $m * (60 * 1000);
      $s = floor(($workVal) / 1000);
      $workVal -= $s * 1000;
      $ms = ($workVal) % 1000;

      $str = "";
      $str .= ($h != 0) ? "${h}h " : "";
      $str .= ($m != 0) ? "${m}m " : "";
      $str .= ($s != 0) ? "${s}s " : "";
      $str .= ($ms != 0) ? "${ms}ms" : "";
      return $str;
   }
}
