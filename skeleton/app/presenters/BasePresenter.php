<?php

namespace App\Presenters;

use Nette\Application\UI\Presenter;
use App\Model\ConstModel;
use Filters;

abstract class BasePresenter extends Presenter
{
   /**
    * @var string
    */
   public $basePath;
   /**
    * @var string
    */
   public $cssVersionSlug;
   /**
    * @var string
    */
   protected $currentPageTitle;
   
   /** @persistent */
   public $locale;
   
   /** @var \Kdyby\Translation\Translator @inject */
   public $translator;
   
   const LANG = 'es';
   /**
    * The displayed version of the page
    * @var string
    */
   const VERSION = 'dev - 1.0.1';
   
   public function __construct()
   {
      //this forces all js and css to reload every single time version changes
      $this->cssVersionSlug = str_replace(' ', '_', self::VERSION);
      
   }
   
   //*******************************************************
   public function beforeRender()
   {
      $this->basePath = $this->getHttpRequest()->getUrl()->getBasePath();
      if (strlen($this->basePath) > 0)  //remove slash at end
         $this->basePath = rtrim($this->basePath, "/");
      
      
      parent::beforeRender();
      
      //general constants useable even for another templates
      $this->template->pageTitle = 'Darkness or Light';
      $this->template->pageUrl = 'http://darknessorlight.glaser.cz';
      $this->template->creatorName = 'Bc. Jan Glaser';
      
      $this->template->cssVersionSlug = $this->cssVersionSlug;
      $this->template->playGameUrl = 'https://darknessorlight.herokuapp.com';
      
      //version of the webpage
      $this->template->VERSION = self::VERSION;
      
      //***************************************************
      $this->template->setTranslator($this->translator);
      
      $user = $this->getUser();
      $this->template->homeLink = $this->link(':Homepage:');
      
      $this->template->currentPageTitle = $this->currentPageTitle;
   }
   
   /**
    * @return \Nette\Application\UI\ITemplate
    */
   protected function createTemplate()
   {
      $template = parent::createTemplate();
      /** @noinspection PhpUndefinedMethodInspection */
      $template->addFilter(NULL, 'Filters::common');
      return $template;
   }
   
   public function translate(string $var): string
   {
      return $this->translator->translate("translator.$var");
   }
}


