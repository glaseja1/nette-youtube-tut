<?php

namespace App\GameManagementModule\TEMPLATE_Module\Model\DAO;

/**
 * Used for getting data related to prayer from database
 */
class TEMPLATE_DAO
{
   /**
    * @var string
    */
   public $codeClass = null;
   /**
    * @var string
    */
   public $description = '';
   /**
    * @var string
    */
   public $displayName = 'undefined';
   /**
    * @var int
    */
   public $id = -1;
   /**
    * @var int
    */
   public $levelReq = 1;
   /**
    * @var bool
    */
   public $overhead = false;
   /**
    * @var int
    */
   public $pointConsumptionMs = 1000;
   /**
    * @var string
    */
   public $resourceDir;
   
   public function __construct()
   {
   }
   
   /**
    * @param array $values
    * @return TEMPLATE_DAO
    */
   public static function createFromFormValues(array $values): TEMPLATE_DAO
   {
      $a = new TEMPLATE_DAO();
      $a->levelReq = $values['levelReq'];
      $a->displayName = $values['displayName'];
      $a->resourceDir = $values['resourceDir'];
      $a->overhead = ($values['overhead'] == 1);
      $a->pointConsumptionMs = $values['pointConsumMs'];
      $a->codeClass = $values['inCodeTAG'];
      return $a;
   }
}
