<?php
namespace App\UserModule\Presenters;

use Nette;
use Nette\Application\UI\Presenter;
use App\Presenters\BasePresenter;
use App\UserModule\Model\UserModel;
use App\UserModule\Model\FormFactory;
use \Nette\Application\UI\Form;
use App\UserModule\Model\DAO\UserDAO;
use App\Model\FunctionReturn;

class HomePresenter extends BasePresenter
{
  /**
   * @var FormFactory
   */
  private $factory;
  /**
   * @var UserModel
   */
  private $modelUser;
  public function __construct(
    UserModel $modelUser,
    FormFactory $factory)
  {
    parent::__construct();

    $this->modelUser = $modelUser;
    $this->factory = $factory;
  }
  public function actionDelete($id)
  {
    $res /*FunctionReturn*/ = $this->modelUser->delete($id);
    if($res->hasErrors())
      $this->flashMessage('Error', 'error');
    else
      $this->flashMessage('Ok');
    $this->redirect(':Default');
  }
  public function createComponentAddForm(): Form
  {
    $a = $this->factory->createAddForm();
    $a->onSuccess[] = [$this, 'formSucceededAdd'];
    return $a;
  }
  public function formSucceededAdd(Form $form, array $values)
  {
    $dao = UserDAO::createFromFormValues($values);

    $res /*FunctionReturn*/ = $this->modelUser->insert($dao);
    if($res->hasErrors())
      $this->flashMessage('Error', 'error');
    else
      $this->flashMessage('Ok');
    $this->redirect(':Default');
  }
  public function renderDefault()
  {
    $this->template->daos = $this->modelUser->findAll();
  }
  public function startup()
  {
    parent::startup();

    $this->template->setFile(__DIR__ . '/templates/home.latte');
  }
}
