drop table if exists `users`;
create table `users`(
  `id` int unsigned auto_increment primary key,
  `username` nvarchar(100),
  `email` nvarchar(100)
) character set utf8 collate utf8_general_ci, engine=INNODB;
