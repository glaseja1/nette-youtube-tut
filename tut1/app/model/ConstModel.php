<?php
namespace App\Model;

class ConstModel
{
   /**
    * All available language mutations
    * @var array
    */
   const LOCALE_ALL = ['en', 'es', 'jap'];
   /**
    * Locale available to the visitor
    * @var array
    */
   const LOCALE_AVALILABLE = ['en', 'es'];
   
   
   const RELOAD_ALL_CSS_JS = true;
}


