<?php
require __DIR__ . '/../vendor/autoload.php';

# Upraví chování PHP a zapne některé vlastnosti Testeru
Tester\Environment::setup();

$configurator = new Nette\Configurator;

// $configurator->enableTracy(__DIR__ . '/../log');

$configurator->setTimeZone('Europe/Prague');
$configurator->setTempDirectory(__DIR__ . '/../temp');

$configurator->createRobotLoader()
    ->addDirectory(__DIR__ . '/../app')
    ->register();

$configurator->addConfig(__DIR__ . '/../app/config/5456414.neon');

$container = $configurator->createContainer();

return $container;
