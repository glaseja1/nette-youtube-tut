<?php
namespace App\ExampleModule\Presenters;

use Nette;
use Nette\Application\UI\Form;
use Nette\Utils\Html;
use Nette\Http\Session;

//presenters
use \App\Presenters\BasePresenter;


class HomePresenter extends BasePresenter
{
   public function __construct()
   {
      parent::__construct();
   }

   public function startup()
   {
      parent::startup();

      //here you can check the ACL
      if (!$this->getUser()->isAllowed('exampleAcl', 'edit')) {

         //redirect, or throw exception....
         throw new Nette\Application\ForbiddenRequestException;
      }

      $this->template->setFile(__DIR__ . "/templates/default.latte");
   }

   public function beforeRender()
   {
      parent::beforeRender();
   }

   public function renderDefault()
   {
   }
}


