<?php

namespace App\GameManagementModule\TEMPLATE_Module\Model;

use App\Model\BaseModel;
use App\Model\FunctionReturn;
use App\Model\DbsConstModel;
use Nette\Database\Context;

//models
use App\GameManagementModule\TEMPLATE_Module\Model\DAO\TEMPLATE_DAO;


/**
 */
class TEMPLATE_Model extends BaseModel
{
   /**
    * constructor.
    * @param Context $database
    * @param DbsConstModel $s
    */
   public function __construct(
       Context $database,
       DbsConstModel $s)
   {
      parent::__construct($database, $s);
   }
   
   //***************************************************************
   
   /**
    * @param array $row
    * @return TEMPLATE_DAO
    */
   public function createDaoFromRow($row): TEMPLATE_DAO
   {
      $dao = new TEMPLATE_DAO();
      $dao->id = $row[$this->getDBColumn($this->sqlNamesModel->dbsPrayersJSON, 'ID')];
      $dao->levelReq = $row[$this->getDBColumn($this->sqlNamesModel->dbsPrayersJSON, 'LEVEL')];
      $dao->displayName = $row[$this->getDBColumn($this->sqlNamesModel->dbsPrayersJSON, 'NAME')];
      $dao->description = $row[$this->getDBColumn($this->sqlNamesModel->dbsPrayersJSON, 'DESC')];
      $dao->resourceDir = $row[$this->getDBColumn($this->sqlNamesModel->dbsPrayersJSON, 'RES_DIR')];
      $dao->overhead = ($row[$this->getDBColumn($this->sqlNamesModel->dbsPrayersJSON, 'OVERHEAD')] == 1);
      $dao->pointConsumptionMs = $row[$this->getDBColumn($this->sqlNamesModel->dbsPrayersJSON, 'CONSTUMPTION')];
      $dao->codeClass = $row[$this->getDBColumn($this->sqlNamesModel->dbsPrayersJSON, 'CODE_CLASS')];
      return $dao;
   }
   
   /**
    * @param $id
    * @return FunctionReturn
    */
   public function delete($id): FunctionReturn
   {
      $ret = new FunctionReturn();
      $T_ = $this->getDBTable($this->sqlNamesModel->dbsPrayersJSON);
      $ID = $this->getDBColumn($this->sqlNamesModel->dbsPrayersJSON, 'ID');
      
      $query = "delete from {$T_} where {$ID}=?";
      $result = $this->database->query($query, $id);
      return $ret;
   }
   
   /**
    * @param $id
    * @return bool
    */
   public function existsByID($id): bool
   {
      return $this->find($id) !== null;
   }
   
   /**
    * Returns all the entities from the database.
    * @return TEMPLATE_DAO[]
    */
   public function findAll(): array
   {
      $query = "select * from {$this->getDBTable($this->sqlNamesModel->dbsPrayersJSON)}";
      
      $result = $this->database->query($query);
      if ($result->getRowCount() === 0)
         return array();
      
      $result = $result->fetchAll();
      
      $resArr = array();
      foreach ($result as $row)
         $resArr[] = $this->createDaoFromRow($row);
      return $resArr;
   }
   
   /**
    * Returns entity by ID. If no such monster exists, returns null
    * @return TEMPLATE_DAO           can be null
    */
   public function find($id)
   {
      $query = "select * from {$this->getDBTable($this->sqlNamesModel->dbsPrayersJSON)}
          where
          {$this->getDBColumn($this->sqlNamesModel->dbsPrayersJSON, 'ID')}=?";
      
      $result = $this->database->query($query, $id);
      if ($result->getRowCount() === 0)
         return null;
      return $this->createDaoFromRow($result->fetch());
   }
   
   /**
    * @param TEMPLATE_DAO $dao
    * @return FunctionReturn
    */
   public function insert(TEMPLATE_DAO $dao): FunctionReturn
   {
      $ret = new FunctionReturn();
      $T_ = $this->getDBTable($this->sqlNamesModel->dbsPrayersJSON);
      
      $query = "insert into {$T_}(
      {$this->getDBColumn($this->sqlNamesModel->dbsPrayersJSON,'LEVEL')},
      {$this->getDBColumn($this->sqlNamesModel->dbsPrayersJSON,'NAME')},
      {$this->getDBColumn($this->sqlNamesModel->dbsPrayersJSON,'RES_DIR')},
      {$this->getDBColumn($this->sqlNamesModel->dbsPrayersJSON,'OVERHEAD')},
      {$this->getDBColumn($this->sqlNamesModel->dbsPrayersJSON,'CONSTUMPTION')},
      {$this->getDBColumn($this->sqlNamesModel->dbsPrayersJSON,'CODE_CLASS')},
      {$this->getDBColumn($this->sqlNamesModel->dbsPrayersJSON,'DESC')}
      ) values(?,?,?,?,?,?,?,?)";
      
      $result = $this->database->query($query,
          $dao->levelReq,
          $dao->displayName,
          $dao->resourceDir,
          $dao->overhead ? 1 : 0,
          $dao->pointConsumptionMs,
          $dao->codeClass,
          $dao->description);
      $ret->addData('id', $this->database->getInsertId());
      return $ret;
   }
   
   /**
    * Updates entity of specific ID
    *
    * @param $id mixed id of the entity to update
    * @param TEMPLATE_DAO $dao data to update with
    * @return FunctionReturn
    * @throws \Error
    */
   public function update($id, TEMPLATE_DAO $dao): FunctionReturn
   {
      $ret = new FunctionReturn();
      if (!$this->existsByID($id)) {
         $ret->addError("Prayer with that id does not exist");
         return $ret;
      }
      
      $T_ = $this->getDBTable($this->sqlNamesModel->dbsPrayersJSON);
      $query = "update {$T_} set
      {$this->getDBColumn($this->sqlNamesModel->dbsPrayersJSON,'LEVEL')}=?,
      {$this->getDBColumn($this->sqlNamesModel->dbsPrayersJSON,'NAME')}=?,
      {$this->getDBColumn($this->sqlNamesModel->dbsPrayersJSON,'RES_DIR')}=?,
      {$this->getDBColumn($this->sqlNamesModel->dbsPrayersJSON,'OVERHEAD')}=?,
      {$this->getDBColumn($this->sqlNamesModel->dbsPrayersJSON,'CONSTUMPTION')}=?,
      {$this->getDBColumn($this->sqlNamesModel->dbsPrayersJSON,'CODE_CLASS')}=?,
      {$this->getDBColumn($this->sqlNamesModel->dbsPrayersJSON,'DESC')}=?
      where {$this->getDBColumn($this->sqlNamesModel->dbsPrayersJSON,'ID')}=?";
      
      $result = $this->database->query($query,
          $dao->levelReq,
          $dao->displayName,
          $dao->resourceDir,
          $dao->overhead ? 1 : 0,
          $dao->pointConsumptionMs,
          $dao->codeClass,
          $dao->description,
          $id);
      return $ret;
   }
}
