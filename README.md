# Nette youtube tutorial

This project contains file for the nette youtube tutorial

## Disclaimer
I will try to clean always the latest code, but I will not be 
beautifying and cleaning code from the previous tutorials,
so apologies, if there are some code artefacts or missing type somewhere

I am using php of certain version (7.something), and I can not write `void` return type,
not mixed or nullable types, such as 

```php
public function a(): void
{
    
}
```

So I do not write there the type, but you should, if you can.

Nullable type allows the variable to be null up to its other type.
```php
function answer(): ?ClassA  {
    return 42; // ok
}
function answer(): ?ClassA  {
    return null; // ok
}
function answer(): ClassA  {
    return null; // error, null is not type of ClassA
}
```
But because of version, I can not use this, so it is not in the project. 
Instead, if returned variable may be null, I ommit the type as 
```php
function answer()  {
    return null; // ok
}
```
But you should use it, if you can to make the code nice

## Skeleton
You can find this in `skeleton` directory. This contains the whole functional minimal webpage,
from which you can start your creation.

If you want to follow along with the first tutorial, then start writing the code to this directory project

## tut1
You can find this in `tut1` directory. This contains the webpage you should have created after watching the first tutorial,
available [here](https://youtu.be/EFgu8GYi6qM)

# Other sources
My website can be found [here](http://honza.glaser.cz)

[Official Nette framework web](https://nette.org/)

[Official Nette framework API documentation](https://api.nette.org/3.0/)