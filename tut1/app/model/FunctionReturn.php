<?php
namespace App\Model;

class FunctionReturn
{
  private $data = [];
  private $errors = [];
  public function __construct()
  { }
  public function addData(string $key, $value)
  {
    $this->data[$key] = $value;
  }

  public function addError(string $e)
  {
    $this->errors[] = $e;
  }

  public function getData(string $key)
  {
    return $this->data[$key];
  }
  
  public function hasErrors(): bool
  {
    return count($this->errors) !== 0;
  }
}
