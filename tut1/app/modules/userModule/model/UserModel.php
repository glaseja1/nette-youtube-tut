<?php
namespace App\UserModule\Model;

use Nette\Database\Context;
use App\Model\BaseModel;
use App\UserModule\Model\DAO\UserDAO;
use App\Model\FunctionReturn;
use Nette\Database\Row;

class UserModel extends BaseModel
{
  public function __construct(Context $database)
  {
    parent::__construct($database);
  }
  private function createDaoFromRow(Row $row): UserDAO
  {
    $dao = new UserDAO();
    $dao->id = $row['id'];
    $dao->email = $row['email'];
    $dao->username = $row['username'];
    return $dao;
  }
  /**
   */
  public function delete($id): FunctionReturn
  {
    $ret = new FunctionReturn();

    $T = 'users';
    $query = "delete from {$T} where `id`=?";

    $this->database->query($query, $id);
    return $ret;
  }
  /**
   * @return UserDAO[]
   */
  public function findAll(): array
  {
    $ret = array();

    $T = 'users';
    $query = "select * from {$T}";

    $result = $this->database->query($query);
    if($result->getRowCount() === 0)
      return $ret;

    $result = $result->fetchAll();
    foreach ($result as $row)
      $ret[] = $this->createDaoFromRow($row);

    return $ret;
  }
  /**
   * Inserts a new user to the database
   * @return
   * @param  UserDAO $dao
   */
  public function insert(UserDAO $dao): FunctionReturn
  {
    $r = new FunctionReturn();

    $T = 'users';
    $query = "insert into {$T}(
      email,
      username
      ) values(?,?)";

    $this->database->query($query,
      $dao->email,
      $dao->username
    );
    $r->addData('id', $this->database->getInsertId());
    return $r;
  }
}
