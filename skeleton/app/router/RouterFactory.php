<?php

namespace App;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;
use App\Model\ConstModel;


class RouterFactory
{
   use Nette\StaticClass;
   
   /**
    * @return Nette\Application\IRouter
    */
   public static function createRouter()
   {
      $router = new RouteList();
      $localeOpts = '';
      for ($i = 0; $i < count(ConstModel::LOCALE_AVALILABLE); $i++) {
         $localeOpts .= ConstModel::LOCALE_AVALILABLE[$i];
         if ($i !== count(ConstModel::LOCALE_AVALILABLE) - 1)
            $localeOpts .= '|';
      }
      
      $router[] = new Route("[<locale=en {$localeOpts}>/]<presenter>/<action>[/<id>]", "Homepage:default");
      return $router;
   }
}
